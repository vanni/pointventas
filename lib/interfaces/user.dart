class User {
  String id;
  String nombre;
  String email;
  String password;
  String puesto;
  String edad;
  String telefono;
  String tipo_usuario;

  User(this.id,this.nombre, this.email, this.password, this.puesto, this.edad, this.telefono, this.tipo_usuario);

  factory User.fromJson(Map<String, dynamic> parsedJson){
      
      var id = parsedJson['id']!=null?parsedJson['id']:"";
      var nombre = parsedJson['nombre']!=null?parsedJson['nombre']:"";
      var email = parsedJson['email']!=null?parsedJson['email']:"";
      var password = parsedJson['password']!=null?parsedJson['password']:"";
      var puesto = parsedJson['puesto']!=null?parsedJson['puesto']:"";
      var edad = parsedJson['edad']!=null?parsedJson['edad']:"";
      var telefono = parsedJson['telefono']!=null?parsedJson['telefono']:"";
      var tipo_usuario = parsedJson['tipo_usuario']!=null?parsedJson['tipo_usuario']:"";

    return User(
      id,
      nombre, 
      email,
      password,
      puesto,
      edad,
      telefono,
      tipo_usuario
    );
  }
  Map<String, dynamic> toJson(user) =>
    {
      "nombre": user.nombre.toString(),
      "email": user.email.toString(),
      "password": user.password.toString(),
      "puesto": user.puesto.toString(),
      "edad": user.edad.toString(),
      "telefono": user.telefono.toString(),
      "tipo_usuario": user.tipo_usuario.toString(),
    };
}
