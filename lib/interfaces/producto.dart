class Producto {
  String id;
  String nombre;
  String precio;
  String stock;
  String descripcion;
  String sku;
  

  Producto(this.id,this.nombre, this.precio, this.stock, this.descripcion, this.sku);

  factory Producto.fromJson(Map<String, dynamic> parsedJson){
      
      var id = parsedJson['id']!=null?parsedJson['id']:"";
      var nombre = parsedJson['nombre']!=null?parsedJson['nombre']:"";
      var precio = parsedJson['precio']!=null?parsedJson['precio']:"";
      var stock = parsedJson['stock']!=null?parsedJson['stock']:"";
      var descripcion = parsedJson['descripcion']!=null?parsedJson['descripcion']:"";
      var sku = parsedJson['sku']!=null?parsedJson['sku']:"";


    return Producto(
      id,
      nombre, 
      precio,
      stock,
      descripcion,
      sku,
    );
  }
  Map<String, dynamic> toJson(Producto) =>
    {
      "nombre": Producto.nombre.toString(),
      "precio": Producto.precio.toString(),
      "stock": Producto.stock.toString(),
      "descripcion": Producto.descripcion.toString(),
      "sku": Producto.sku.toString(),
      
    };
}
