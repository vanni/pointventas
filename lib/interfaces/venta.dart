
class Venta {
  String id;
  String cliente;
  String telefono;
  String comentario;
  String correo;
  String direccion;
  String estatus;
  String paqueteria;
  String guia;
  int total;
  List productos;

  Venta(
    this.id,
    this.cliente,
    this.telefono, 
    this.comentario, 
    this.correo, 
    this.direccion, 
    this.estatus, 
    this.paqueteria, 
    this.guia, 
    this.total,
    this.productos);

  factory Venta.fromJson(Map<String, dynamic> parsedJson) {
    var id = parsedJson['id'] != null ? parsedJson['id'] : "";
    var cliente = parsedJson['cliente'] != null ? parsedJson['cliente'] : "";
    var telefono = parsedJson['telefono'] != null ? parsedJson['telefono'] : "";
    var comentario =parsedJson['comentario'] != null ? parsedJson['comentario'] : "";
    var correo = parsedJson['correo'] != null ? parsedJson['correo'] : "";
    var direccion =parsedJson['direccion'] != null ? parsedJson['direccion'] : "";
    var estatus = parsedJson['estatus'] != null ? parsedJson['estatus'] : "";
    var paqueteria = parsedJson['paqueteria'] != null ? parsedJson['paqueteria'] : "";
    var guia = parsedJson['guia'] != null ? parsedJson['guia'] : "";
    var total = parsedJson['total'] != null ? parsedJson['total'] : "";
    var productos =parsedJson['productos'] != null ? parsedJson['productos'] : [];
    return Venta(
      id,
      cliente,
      telefono,
      comentario, 
      correo, 
      direccion, 
      estatus, 
      paqueteria, 
      guia, 
      total,
      productos);
  }
  Map<String, dynamic> toJson(Venta) => {
        "cliente": Venta.cliente.toString(),
        "telefono": Venta.telefono.toString(),
        "comentario": Venta.comentario.toString(),
        "correo": Venta.correo.toString(),
        "direccion": Venta.direccion.toString(),
        "estatus": Venta.estatus.toString(),
        "paqueteria": Venta.paqueteria.toString(),
        "guia": Venta.guia.toString(),
        "total": Venta.total,
        "productos": Venta.productos,
      };
}
