import 'package:intl/intl.dart';
class Registro {
  String nombre;
  String email;
  String puesto;
  String edad;
  String telefono;
  DateTime fechaHora;
  String temperatura;

  Registro(this.nombre, this.email,this.puesto, this.edad,this.telefono, this.fechaHora,this.temperatura );
  
  factory Registro.fromJson(Map<String, dynamic> parsedJson){
    String fecha = parsedJson['fechaHora'].toString().replaceFirst(RegExp('T'), ' ', 1).replaceFirst(RegExp('Z'), '', 1);

    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    String string = dateFormat.format(DateTime.now());
    DateTime dateTime = dateFormat.parse(fecha);
    return Registro(
      parsedJson['nombre'], 
      parsedJson['email'],
      parsedJson['puesto'],
      parsedJson['edad'].toString(),
      parsedJson['telefono'],
      dateTime,
      parsedJson['temperatura'].toString(),

    );
  }
 
}
