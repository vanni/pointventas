import 'dart:convert';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:ventas/interfaces/producto.dart';
import 'package:ventas/interfaces/user.dart';
import 'package:ventas/pages/editProduct.dart';
import 'package:ventas/pages/productos.dart';
import 'package:ventas/pages/registro_usuario.dart';


void main() => runApp(const AdministrarProductos());

/// This is the main application widget.
class AdministrarProductos extends StatelessWidget {
  const AdministrarProductos({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        body: MyStatelessWidget(),
      ),
    );
  }
}


class Usert{
  String name;
  int age;
  String role;

  Usert(this.name, this.age, this.role);
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatefulWidget {
  
  MyStatelessWidget({Key? key}) : super(key: key);

  @override
  _MyStatelessWidgetState createState() => _MyStatelessWidgetState();
}

class _MyStatelessWidgetState extends State<MyStatelessWidget> {
  List<Producto> userst = [];
  List<Producto> usersFiltered = [];
  List<Producto> usersList = [];
  TextEditingController controller = TextEditingController();
  String _searchResult = '';
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("productos");
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsers();
  }
  Future getUsers() async{
    controller.clear();
    QuerySnapshot users= await collectionReference.get();
    List<Producto> lista = [];
    setState(() {
      controller.clear();
      _searchResult = '';
      if(users.docs.length != 0){
      users.docs.forEach((element) async {
        print(element.data());
          Producto  usrpas=await Producto.fromJson(element.data()! as Map<String, dynamic>); 
          print(usrpas);
          usrpas.id=element.id;
          lista.add(usrpas);

        });
      }
        usersList=lista;
        usersFiltered=lista;

      });
    
  }

  Future update(Producto userPast) async {
    print("me mandaste"+userPast.nombre.toString());

    Navigator.push(
      context, new MaterialPageRoute(builder: (context) => ProductosEdit(userPast))
    );
    
  }
  Future eliminar(Producto p) async {
    await collectionReference.doc(p.id).delete();
    

    var data = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Mensaje!!"),
          content: new Text("El producto se eliminado con exito"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => AdministrarProductos()));
  } 
/*
  Future getUsers() async {
    var url = Uri.parse('http://localhost:8080/users');
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8'
    });
    var data = json.decode(res.body) as List;
    setState(() {
      controller.clear();
      _searchResult = '';
      List<User> lista = [];
      data.forEach((element) {
      print(element);
      lista.add(User.fromJson(element));
      });
      usersFiltered = lista;
      usersList = lista;

    });

  }
  @override
  void initState() {
    super.initState();
    getUsers();

  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
        
        Positioned(
          top: 0,
          
          width:  MediaQuery.of(context).size.width,
          child: SvgPicture.asset(
            'images/w1.svg',
            width:  MediaQuery.of(context).size.width,
          
            fit: BoxFit.fill,
          )),
          SizedBox(
                height: 50,
              ),
        
                Text(
                  "Productos",
                  style: GoogleFonts.pacifico(
                      fontWeight: FontWeight.bold,
                      fontSize: 50,
                      color: Colors.blue),
                ),
                SizedBox(
                  height: 25,
                ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center ,
          
          children: [
            FlatButton(
              color: Colors.blue,

              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),),
              onPressed: () {
                Navigator.push(
                  context, new MaterialPageRoute(builder: (context) => Productos()));
              },
              child: Text(
                "Crear",
                style: TextStyle(color: Colors.white, fontSize: 20),
              )),
              SizedBox(
                  height: 5,
              ),
              
              
                /*FlatButton(
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              onPressed: () {
                Navigator.push(
                  context, new MaterialPageRoute(builder: (context) => UsuarioAdd()));
              },
              child: Text(
                "Ver Entradas",
                style: TextStyle(color: Colors.white, fontSize: 20),
              )),*/
          ],
        ),
        SizedBox(
                height: 10,
              ),
        Card(
          
          child: new ListTile(
            leading: new Icon(Icons.search),
            title: new TextField(
                controller: controller,
                decoration: new InputDecoration(
                    hintText: 'Buscar', border: InputBorder.none),
                onChanged: (value) {
                  setState(() {
                    _searchResult = value;
                    usersFiltered = usersList.where((user) => user.nombre.contains(_searchResult) || user.nombre.contains(_searchResult)).toList();
                  });
                }),
            trailing: new IconButton(
              icon: new Icon(Icons.cancel),
              onPressed: () {
                setState(() {
                  controller.clear();
                  _searchResult = '';
                  usersFiltered = usersList;
                });
              },
            ),
          ),
        ),
        
        Container(
          width:  MediaQuery.of(context).size.width,

          child: SingleChildScrollView(
            child: 
              DataTable(
                columnSpacing: (MediaQuery.of(context).size.width / 30) * 0.5,
                columns: const <DataColumn>[
                  
                  DataColumn(
                    label: Text(
                      'nombre',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'precio',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'SKU',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Editar',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent ),
                      
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Delete',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                ],
                rows: List.generate(usersFiltered.length, (index) =>
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(usersFiltered[index].nombre)),
                        DataCell(Text(usersFiltered[index].precio)),
                        DataCell(Text(usersFiltered[index].sku)),
                        DataCell(
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              update(usersFiltered[index]);
                            },
                          ),
                        ),
                        DataCell(
                          IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              eliminar(usersFiltered[index]);

                            },
                          ),
                        ),
                      ],
                    ),
                ),
            )
      )),
      ],
      ))));
  }
}