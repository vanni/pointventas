import 'dart:convert';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:ventas/interfaces/producto.dart';
import 'package:ventas/interfaces/user.dart';
import 'package:ventas/interfaces/venta.dart';
import 'package:ventas/pages/productos.dart';
import 'package:ventas/pages/registro_usuario.dart';


void main() => runApp(const DashProductos());

/// This is the main application widget.
class DashProductos extends StatelessWidget {
  const DashProductos({Key? key}) : super(key: key);
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        body: MyStatelessWidget(),
      ),
    );
  }
}


class Usert{
  String name;
  int age;
  String role;

  Usert(this.name, this.age, this.role);
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatefulWidget {
  
  MyStatelessWidget({Key? key}) : super(key: key);

  @override
  _MyStatelessWidgetState createState() => _MyStatelessWidgetState();
}

class _MyStatelessWidgetState extends State<MyStatelessWidget> {
  List<Producto> userst = [];
  List<Producto> prodSel = [];
  List<Producto> usersFiltered = [];
  List<Producto> usersList = [];
  var total=0;
  Venta venta = new Venta("","","","","","","en_revicion","","",0,[]);
  TextEditingController controller = TextEditingController();
  String _searchResult = '';
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("productos");
  CollectionReference collectionReferenceVenta = FirebaseFirestore.instance.collection("ventas");
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsers();
  }
  Future getUsers() async{
    controller.clear();
    QuerySnapshot users= await collectionReference.get();
    List<Producto> lista = [];
    setState(() {
      controller.clear();
      _searchResult = '';
      if(users.docs.length != 0){
        int index=0;
      
        users.docs.forEach((element) async {
          print(element.data());
            Producto  usrpas=await Producto.fromJson(element.data()! as Map<String, dynamic>); 
            print(usrpas);
            lista.add(usrpas);
            index++;

          });
      }
        usersList=lista;
        usersFiltered=lista;

      });
    
  }

  Future nextStep() async {
    List listProducts=[];
    prodSel.forEach((element) {
      listProducts.add(element.nombre);
    });
    venta.productos=listProducts;
    venta.total=total;
    var data = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Datos de venta"),
          content: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text:  venta.cliente),
                    enabled: true, 
                    onChanged: (value) {
                      venta.cliente = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nombre cliente';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        
                        hintText: 'Ingresa un cliente',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: venta.correo),
                    onChanged: (value) {
                      venta.correo = value;
                      
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'ingresa un email';
                      } else if (RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value)) {
                        return null;
                      } else {
                        return 'Email invalido';
                      }
                    },
                    decoration: InputDecoration(

                        hintText: 'Ingresa un Email',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: venta.telefono),
                    onChanged: (value) {
                        venta.telefono = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un telefono';
                      }
                      return null;
                    },
                    decoration: InputDecoration(

                        hintText: 'Telefono del cliente',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))
                    ),
                  ),
                ),
                
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text:  venta.direccion),
                    enabled: true, 
                    onChanged: (value) {
                      venta.direccion = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un dirección';
                      }
                      return null;
                    },
                    decoration: InputDecoration(

                        hintText: 'Ingresa una dirección',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                /*Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text:  venta.comentario),
                    enabled: true, 
                    minLines: 4, // any number you need (It works as the rows for the textarea)
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    onChanged: (value) {
                      venta.comentario = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un comentario';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        
                        hintText: 'Comentario',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),*/
                
                
          ]))),
          
          actions: <Widget>[
            new FlatButton(
              child: new Text("Registrar"),
              onPressed: () {
                save();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    
  }
  Future save() async {
    var resp= await collectionReferenceVenta.add(venta.toJson(venta));
    print(venta.productos);

    setState(() {
      total=0;
      Venta ventaClean = new Venta("","","","","","","","","en_revicion",0,[]);
      venta=ventaClean;
      prodSel=[];
      controller.clear();
    });
  } 
  Future eliminar(Producto p) async {

    setState(() {
      controller.clear();
      print(p);
      total=total-int.parse(p.precio);
      _searchResult = '';
      prodSel.remove(p);
    

    });
  } 
  Future add(Producto p) async{
    setState(() {
      controller.clear();
      _searchResult = '';
    prodSel.add(p);
    total=total+int.parse(p.precio);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
        
        Positioned(
          top: 0,
          
          width:  MediaQuery.of(context).size.width,
          child: SvgPicture.asset(
            'images/w1.svg',
            width:  MediaQuery.of(context).size.width,
          
            fit: BoxFit.fill,
          )),
        
        Column(
          crossAxisAlignment: CrossAxisAlignment.center ,
          
          children: [
              SizedBox(
                  height: 5,
              ),
              
              
          ],
        ),
        SizedBox(
                height: 10,
              ),
        Card(
          
          child: new ListTile(
            leading: new Icon(Icons.search),
            title: new TextField(
                controller: controller,
                decoration: new InputDecoration(
                    hintText: 'Buscar', border: InputBorder.none),
                onChanged: (value) {
                  setState(() {
                    _searchResult = value;
                    usersFiltered = usersList.where((user) => user.nombre.contains(_searchResult) || user.nombre.contains(_searchResult)).toList();
                  });
                }),
            trailing: new IconButton(
              icon: new Icon(Icons.cancel),
              onPressed: () {
                setState(() {
                  controller.clear();
                  _searchResult = '';
                  usersFiltered = usersList;
                });
              },
            ),
          ),
        ),
        
        Container(
          width:  MediaQuery.of(context).size.width,

          child: SingleChildScrollView(
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              
              DataTable(
                columnSpacing: (MediaQuery.of(context).size.width / 30) ,
                columns: const <DataColumn>[
                  
                  DataColumn(
                    label: Text(
                      'nombre',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'precio',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'SKU',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      ' Descripción',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent ),
                      
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Delete',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                ],
                rows: List.generate(usersFiltered.length, (index) =>
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(usersFiltered[index].nombre)),
                        DataCell(Text(usersFiltered[index].precio)),
                        DataCell(Text(usersFiltered[index].sku)),
                        DataCell(Text(usersFiltered[index].descripcion)),
                        
                        DataCell(
                          IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () {
                              add(usersFiltered[index]);

                            },
                          ),
                        ),
                      ],
                    ),
                ),
            ),
            SizedBox(
                height: 50,
            ),
            Text( 
              "Productos selecionados",
              style: Theme.of(context).textTheme.headline5,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                  for(var item in prodSel ) 
                    Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            
                            title: Text(item.nombre),
                            subtitle: Text("\$ "+item.precio),
                            leading: new IconButton(
                              icon: new Icon(Icons.delete),
                              highlightColor: Colors.pink,
                              onPressed:(){
                                  eliminar(item);
                              }
                            )
                          ),
                          
                        ],
                      ),
                    ),
              ],
            ),
            SizedBox(
                height: 10,
            ),
            Text( 
              "Total: "+ total.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(55, 16, 16, 0),
                child: Container(
                  height: 50,
                  width: 400,
                  child: FlatButton(
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16.0)),
                      onPressed: () {
                        nextStep();
                      },
                      child: Text(
                        "Registrar compra",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                ),
              ),
             
            
            
            
            ]
            )
            
        )
      ),
      ],
      ))));
  }
}