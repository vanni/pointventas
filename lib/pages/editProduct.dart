import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:ventas/interfaces/producto.dart';
import 'package:ventas/interfaces/producto.dart';
import 'package:ventas/pages/administrar_productos.dart';

/*nombre del producto, precio, descripción, SKU*/

class ProductosEdit extends StatefulWidget {
  final Producto prod;
  //final String temperatura;


  ProductosEdit(this.prod, {Key? key,}) : super(key: key);
  initState(){
    //print("Me pasaste"+userResponse.tipo_usuario);
    //print("Con temperatura"+temperatura);
  }
  @override
  _ProductosState createState() => _ProductosState(prod);
}

class _ProductosState extends State<ProductosEdit> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("productos");

  final Producto producto;
  //final String temperatura;

  _ProductosState(this.producto);
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");

  Future save() async {
    
    await collectionReference.doc(producto.id).update(producto.toJson(producto));
    var data = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Mensaje"),
          content: new Text("El producto se modificado con exito!"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => AdministrarProductos()));
  }

  //Producto producto = Producto('','', '','','','');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
        child: SingleChildScrollView( child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
  
      /*Text("Productos",
            style: GoogleFonts.pacifico(
                fontWeight: FontWeight.bold, fontSize: 50, color: Colors.blue)),
        ),*/
      Positioned(
          top: 0,
          width:  MediaQuery.of(context).size.width,
          child: SvgPicture.asset(
            'images/w1.svg',
            width:  MediaQuery.of(context).size.width,
          
            fit: BoxFit.fill,
          )),
      Container(
        alignment: Alignment.center,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                "Editar producto",
                style: GoogleFonts.pacifico(
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                    color: Colors.blue),
              ),
              SizedBox(
                height: 25,
              ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text: producto.nombre),
                    enabled: true, 
                    onChanged: (value) {
                      producto.nombre = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Requiere nombre';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.add_shopping_cart  ,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Ingresa un nombre',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text:  producto.precio),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      producto.precio = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Requiere precio';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.attach_money,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'precio',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: producto.stock),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      producto.stock = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Requiere stock';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.library_books_rounded ,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'stock',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: producto.descripcion),
                    onChanged: (value) {
                      producto.descripcion = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Requiere Descripción';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.description_outlined ,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Descripción',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))
                    ),
                  ),
                ),
                  Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    onChanged: (value) {
                      producto.sku = value;
                    },
                    controller: TextEditingController(text: producto.sku),
                    
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Requiere SKU';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.password  ,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'SKU',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
              
              Padding(
                padding: EdgeInsets.fromLTRB(55, 16, 16, 0),
                child: Container(
                  height: 50,
                  width: 400,
                  child: FlatButton(
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16.0)),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          save();
                        } else {
                          print("not ok");
                        }
                      },
                      child: Text(
                        "Guardar producto",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                ),
              )
            ],
          ),
        ),
      )
      
    ])
    )
    )
    );
  }
}
