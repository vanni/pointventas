import 'dart:convert';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:ventas/interfaces/producto.dart';
import 'package:ventas/pages/registro_usuario.dart';
import 'package:ventas/interfaces/venta.dart';

void main() => runApp(const VerificacionVentas());

/// This is the main application widget.
class VerificacionVentas extends StatelessWidget {
  const VerificacionVentas({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        body: MyStatelessWidget(),
      ),
    );
  }
}

class Usert {
  String name;
  int age;
  String role;

  Usert(this.name, this.age, this.role);
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatefulWidget {
  MyStatelessWidget({Key? key}) : super(key: key);

  @override
  _MyStatelessWidgetState createState() => _MyStatelessWidgetState();
}

class _MyStatelessWidgetState extends State<MyStatelessWidget> {
  List<Venta> userst = [];
  List<Venta> usersFiltered = [];
  List<Venta> usersList = [];
  List<Producto> productsVenta = [];
  TextEditingController controller = TextEditingController();
  String _searchResult = '';
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("ventas");
  CollectionReference collectionReferenceProd = FirebaseFirestore.instance.collection("productos");


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsers();
  }

  Future getUsers() async {
    controller.clear();
    QuerySnapshot users = await collectionReference.get();
    List<Venta> lista = [];
    setState(() {
      controller.clear();
      _searchResult = '';
      if (users.docs.length != 0) {
        users.docs.forEach((element) async {
          print(element.data());
          Venta usrpas =
              await Venta.fromJson(element.data()! as Map<String, dynamic>);
          usrpas.id=element.id;
          print(usrpas.id);
          lista.add(usrpas);
        });
      }
      usersList = lista;
      usersFiltered = lista;
    });
  }

  Future update(Venta userPast) async {
    
    
  }


  Future compra_detail(Venta v, List<Producto> list) async {
    print("buscando");
    
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Datos de venta"),
          content: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DataTable(
                columnSpacing: (MediaQuery.of(context).size.width / 20),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      'SKU',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'nombre',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent
                      ),
                    ),
                  ),                  
                  DataColumn(
                    
                    label: Text(
                      'precio',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blueAccent),
                    ),
                  ),
                  
                ],
                rows: List.generate(list.length, (index) =>
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(list[index].sku)),
                        DataCell(Text(list[index].nombre)),
                        DataCell(Text(list[index].precio)),
                        
                      ],
                    ),
                ),
            ),
            SizedBox(
                height: 10,
            ),
            Text( 
              "Total: "+ v.total.toString(),
              style: Theme.of(context).textTheme.headline5,
            ),
            SizedBox(
                height: 30,
            ),Text( 
              "Cliente: "+ v.cliente.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),Text( 
              "Correo: "+ v.correo.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),Text( 
              "Telefono: "+ v.telefono.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),
            Text( 
              "Dirección: "+ v.direccion.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),
            
          ]))),
          
          actions: <Widget>[
            new FlatButton(
              child: new Text("Rechazar"),
              onPressed: () {
                Navigator.of(context).pop();
                denied(v);
              },
            ),
            new FlatButton(
              child: new Text("Aceptar"),
              onPressed: () {
                Navigator.of(context).pop();
                save(v, 2);
                getUsers();
              },
            )
          ],
        );
      },
    );
    
  }
  Future denied(Venta v) async {
    
    var data = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Datos de venta"),
          content: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text:  v.comentario),
                    enabled: true, 
                    minLines: 4, // any number you need (It works as the rows for the textarea)
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    onChanged: (value) {
                      v.comentario = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un comentario';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        
                        hintText: 'Comentario',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
          ]))),
          
          actions: <Widget>[
            
            new FlatButton(
              child: new Text("Guardar"),
              onPressed: () {
                save(v, 1);
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );

    
  }

  Future save(Venta v, tipe) async {
    if(tipe==1)
      await collectionReference.doc(v.id).update({"comentario": v.comentario, "estatus": "rechazada"});
    else 
      await collectionReference.doc(v.id).update({"estatus": "aceptada"});
    getUsers();
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Positioned(
            top: 0,
            width: MediaQuery.of(context).size.width,
            child: SvgPicture.asset(
              'images/w1.svg',
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.fill,
            )),
        SizedBox(
          height: 50,
        ),
        Text(
          "Validación",
          style: GoogleFonts.pacifico(
              fontWeight: FontWeight.bold, fontSize: 50, color: Colors.blue),
        ),
        SizedBox(
          height: 25,
        ),

        Card(
          child: new ListTile(
            leading: new Icon(Icons.search),
            title: new TextField(
                controller: controller,
                decoration: new InputDecoration(
                    hintText: 'Buscar', border: InputBorder.none),
                onChanged: (value) {
                  setState(() {
                    _searchResult = value;
                    usersFiltered = usersList
                        .where((user) =>
                            user.estatus.contains(_searchResult) ||
                            user.comentario.contains(_searchResult))
                        .toList();
                  });
                }),
            trailing: new IconButton(
              icon: new Icon(Icons.cancel),
              onPressed: () {
                setState(() {
                  controller.clear();
                  _searchResult = '';
                  usersFiltered = usersList;
                });
              },
            ),
          ),
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: DataTable(
              columnSpacing: (MediaQuery.of(context).size.width / 30) * 0.5,
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    'No. Referencia',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.blueAccent),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Cliente',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.blueAccent),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Total',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.blueAccent),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Estatus',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.blueAccent),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Ver',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.blueAccent),
                  ),
                ),
              ],
              rows: List.generate(
                usersFiltered.length,
                (index) => DataRow(
                  cells: <DataCell>[
                    DataCell(Text(usersFiltered[index].id)),
                    DataCell(Text(usersFiltered[index].cliente)),
                    DataCell(Text(usersFiltered[index].total.toString())),
                    DataCell(Text(usersFiltered[index].estatus)),
                    
                    DataCell(
                      IconButton(
                        icon: Icon(Icons.remove_red_eye),
                        onPressed: () {
                          List<Producto> aux=[];
                          usersFiltered[index].productos.forEach((e) async {
                          QuerySnapshot p = await collectionReferenceProd
                            .where("nombre", isEqualTo: e)
                            .get();
                          if (p.docs.length != 0) {
                            p.docs.forEach((el) async {
                              Producto ve = await Producto.fromJson(el.data()! as Map<String, dynamic>);
                              aux.add(ve);
                              if(usersFiltered[index].productos.length==aux.length){
                                  compra_detail(usersFiltered[index], aux);
                              }
                            });
                          }
                        });

                        },
                      ),
                    ),
                  ],
                ),
              ),
            ))),
      ],
    );
  }
}
