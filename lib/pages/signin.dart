import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:ventas/interfaces/user.dart';
import 'package:ventas/pages/administrar_trabajadores.dart';
import 'package:ventas/pages/almacenista.dart';
import 'package:ventas/pages/dashboard_compra.dart';
import 'package:ventas/pages/verificacion_ventas.dart';
import 'package:ventas/pages/registro_usuario.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Signin extends StatefulWidget {
  Signin({Key? key}) : super(key: key);

  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection("users");

  final _formKey = GlobalKey<FormState>();
  Future save() async {
    QuerySnapshot userget = await collectionReference
        .where("email", isEqualTo: user.email)
        .where("password", isEqualTo: user.password)
        .get();

    if (userget.docs.length != 0) {
      userget.docs.forEach((element) {
        //Map valueMap = json.decode(element.data());
        //print(valueMap);
        User usrpas = User.fromJson(element.data()! as Map<String, dynamic>);
        print(usrpas.email);
        //usrpas={...element.data()};
        if (usrpas.tipo_usuario.compareTo("Administrador") == 0) {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => AsministrarUsuarios()));
        } else if (usrpas.tipo_usuario.compareTo("Encargado mostrador") == 0) {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => DashProductos()));
        } else if (usrpas.tipo_usuario.compareTo("Encargado finanzas") == 0) {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => VerificacionVentas()));
        } else if (usrpas.tipo_usuario.compareTo("Almacenista") == 0) {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => EnvioVentas()));
        } 
        
      });
    } else {
      var data = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Mensaje"),
            content: new Text("Usuario no encontrado!"),
            actions: <Widget>[
              new FlatButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    //var results =  json.decode(res.body);
    //User usResponse = User.fromJson(results);
    //User userRespose = User.fromJson(userget.docs);
    //print(userRespose);

    //print("responde con: "+usResponse.edad);
    /*if(user.tipo.compareTo("Administrador")==0){
      Navigator.push(context, new MaterialPageRoute(builder: (context) => AsministrarUsuarios()));
      
    }else{
      //Navigator.push(context, new MaterialPageRoute(builder: (context) => AsministrarUsuarios()));
    }*/
  }

  User user = User('', '', '', '', '', '', '', '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: SingleChildScrollView(
                child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Positioned(
            top: 0,
            width: MediaQuery.of(context).size.width,
            child: SvgPicture.asset(
              'images/w1.svg',
              width: MediaQuery.of(context).size.width,
              height: 80,
              fit: BoxFit.fill,
            )),
        Container(
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 150,
                ),
                Text(
                  "Login",
                  style: GoogleFonts.pacifico(
                      fontWeight: FontWeight.bold,
                      fontSize: 50,
                      color: Colors.blue),
                ),
                SizedBox(
                  height: 25,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text: user.email),
                    onChanged: (value) {
                      user.email = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un email';
                      } else if (RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value)) {
                        return null;
                      } else {
                        return 'Email incorrecto';
                      }
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.email,
                          color: Colors.blue,
                        ),
                        hintText: 'Ingresa Email',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blue)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blue)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text: user.password),
                    onChanged: (value) {
                      user.password = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Introdusca una contraseña';
                      }
                      return null;
                    },
                    obscureText: true,
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.vpn_key,
                          color: Colors.blue,
                        ),
                        hintText: 'Introdusca una contraseña',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blue)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blue)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(55, 16, 16, 0),
                    child: Flexible(
                      child: Container(
                        height: 50,
                        width: 400,
                        child: FlatButton(
                            color: Colors.blue,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16.0)),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                save();
                              } else {
                                print("not ok");
                              }
                            },
                            child: Text(
                              "Entrar",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            )),
                      ),
                    )),
                
                  
              ],
            ),
          ),
        )
      ],
    ))));
  }
}
