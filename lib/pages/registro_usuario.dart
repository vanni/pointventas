import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:ventas/interfaces/registro.dart';
import 'package:ventas/interfaces/user.dart';
import 'package:ventas/pages/administrar_trabajadores.dart';
/*nombre del producto, precio, descripción, SKU*/

class UsuarioAdd extends StatefulWidget {
  //final User userResponse;
  //final String temperatura;


  UsuarioAdd( {Key? key,}) : super(key: key);
  initState(){
    //print("Me pasaste"+userResponse.tipo_usuario);
    //print("Con temperatura"+temperatura);
  }
  @override
  _UsuarioAddState createState() => _UsuarioAddState();
}

class _UsuarioAddState extends State<UsuarioAdd> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("users");
  //Usuario registro = Registro('', '','','','', new DateTime.now(),'');
  User user = User('', '', '','','','','','');

  String dropdownValue = 'Administrador';
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");

  Future save() async {
    //var url = Uri.parse('http://localhost:8080/registrar_entrada');
    //final cosa={"nombre": "pruebea", "cosa":"cosa"};
    user.tipo_usuario=dropdownValue;

    var resp= await collectionReference.add(user.toJson(user));

    /*var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8'
    }, body: <String, String>{
      'nombre': usPast.nombre,
      'email': usPast.email,
      'puesto': usPast.puesto,
      'edad': usPast.edad,
      'telefono': usPast.telefono,
      'fechaHora': registro.fechaHora.toString(),
      'temperatura': temperatura,r

    });*/

    print(user.tipo_usuario);
    var data = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Mensaje"),
          content: new Text("El registro se aguardado con exito!"),
          actions: <Widget>[
              FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => AsministrarUsuarios()));
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
        child: SingleChildScrollView( 
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
  
      /*Text("UsuarioAdd",
            style: GoogleFonts.pacifico(
                fontWeight: FontWeight.bold, fontSize: 50, color: Colors.blue)),
        ),*/
      Positioned(
          top: 0,
          width:  MediaQuery.of(context).size.width,
          child: SvgPicture.asset(
            'images/w1.svg',
            width:  MediaQuery.of(context).size.width,
          
            fit: BoxFit.fill,
          )),
      Container(
        alignment: Alignment.center,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                "Registro de usuario",
                style: GoogleFonts.pacifico(
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                    color: Colors.blue),
              ),
              SizedBox(
                height: 25,
              ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    controller: TextEditingController(text:  user.nombre),
                    enabled: true, 
                    onChanged: (value) {
                      user.nombre = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un nombre';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.person,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Ingresa un nombre',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: user.email),
                    onChanged: (value) {
                      user.email = value;
                      
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'ingresa un email';
                      } else if (RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value)) {
                        return null;
                      } else {
                        return 'Email invalido';
                      }
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.email,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Ingresa un Email',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    obscureText: true,
                    controller: TextEditingController(text: user.password),
                    onChanged: (value) {
                      user.password = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa una contraseña';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.vpn_key ,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Ingresa una contraseña',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))),
                  ),
                ),
                
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    enabled: true, 
                    controller: TextEditingController(text: user.telefono),
                    onChanged: (value) {
                      user.telefono = value;
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Ingresa un telefono';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.phone,
                          color: Colors.blueAccent,
                        ),
                        hintText: 'Ingresa un telefono',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.blueAccent)),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: BorderSide(color: Colors.red))
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(color: Colors.blue),
                        underline: Container(
                          height: 2,
                          color: Colors.blueAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: <String>['Administrador', 'Encargado mostrador', 'Encargado finanzas', 'Almacenista']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        
                      ),
                ),
                
              
              Padding(
                padding: EdgeInsets.fromLTRB(55, 16, 16, 0),
                child: Center(
                 
                  child: FlatButton(
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16.0)),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          save();
                        } else {
                          print("not ok");
                        }
                      },
                      child: Text(
                        "Registrar Usuario",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                ),
              )
            ],
          ),
        ),
      )
      
    ])
    )
    )
    );
  }
}
